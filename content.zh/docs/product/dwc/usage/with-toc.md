---
title: 指南一：带目录
weight: 1
---
# 语音合成接口 TTS3 说明

中控 WebSocket 全双工接口第三 TTS 调用方式的说明，链接方式为 WebSocket 协议，报文皆为使用 UTF-8 编码的 JSON 文本。

## 调用流程

1. 建立 WebSocket 连接（无需鉴权），如果在本地启动中控，地址通常为 `ws://localhost:8070/v1`；
2. 发送 Starter 包，内容为后续 TTS 请求的通用配置信息，如果格式错误或超过 10 秒未发送会被断开；
3. 发送 Task 包，内容为特定需要合成的文字和格式信息；
4. 收到对应 Task 的语音包和音素包（顺序不保证）；
5. 可以并行发送多个 Task 包，并发限制为供应商处实际 QPS 的限制；
6. 如果 60 秒内没有收到任何请求，服务端会主动断开，建议以一定间隔发送 Ping 包进行保活；
7. 如果当前没有更多语音合成任务，可以直接断开（没有链接断开报文的设计）；

## 请求报文格式

### Starter

每次建立连接后发送的第一个包，表示此连接的目的和后续数据包的解析方式。格式为 JSON 文本，包含以下字段：

| 字段      | 名称          | 类型   | 默认值      | 说明                                                     |
| :-------- | :------------ | :----- | :---------- | :------------------------------------------------------- |
| `auth`    | AuthN Token   | string | 空字符串    | 设备鉴权 Token，如服务端开启鉴权则必填                   |
| `type`    | Workflow Type | string | 必填        | 此处填写`TTS3`                                           |
| `device`  | Device ID     | string | 空字符串    | 设备 ID，建议填写，以便追溯和定位问题                    |
| `session` | Session ID    | string | 随机 UUIDv4 | 建议调用者自行生成 Session ID 并填写，以便追溯和定位问题 |
| `tts`     | TTS Config    | map    | 必填        | TTS 专属配置，具体信息见下                               |

TTS Config 配置见下：

| 字段          | 名称                           | 类型   | 默认值     | 说明                                                                |
| :------------ | :----------------------------- | :----- | :--------- | :------------------------------------------------------------------ |
| `language`    | Language Code                  | string | `zh-CN`    | 语言编码，目前只支持中文，即 `zh-CN`                                |
| `voice`       | Voice ID                       | string | `xiaoling` | 发音人 ID，可选择的三个发音人为: xiaoling, xiaoyue, xiaoyu          |
| `speed_ratio` | Speed Ratio                    | float  | `1.0`      | 语速，数值越大语速越慢，支持范围 [0.5, 2]                           |
| `sample_rate` | Sample Rate                    | int    | `16000`    | 采样率，支持：8000, 11025, 16000, 22050, 24000, 32000, 44100, 48000 |
| `volume`      | Volume                         | int    | `100`      | 音量，数值越大声音越大，支持范围 [1, 400]                           |
| `phone`       | Return Phonetic Symbols        | bool   | `false`    | 是否返回音素                                                        |
| `omit_error`  | Omit Error Message in Response | bool   | `false`    | 可选字段，是否删去报错信息，即默认会返回                            |

### Task

Starter 包发送并成功建立连接后，后续可重复发送多个 Task 来提交合成任务。

| 字段       | 名称       | 类型   | 默认值     | 说明                                                                                                            |
| :--------- | :--------- | :----- | :--------- | :-------------------------------------------------------------------------------------------------------------- |
| `id`       | Task ID    | string | 随机字符串 | 建议调用者自行生成并填写，用于区分并发请求时不同 Task 的返回                                                    |
| `query`    | Query      | string | 必填       | 待合成语音的文本内容                                                                                            |
| `ssml`     | Use SSML   | bool   | `false`    | 是否使用 SSML 来对合成文本进行标记，写法参考 Confluence 使用文档                                                |
| `override` | TTS Config | map    | 空         | 单条 TTS 请求的独立配置，仅为为当前任务完整替换 Starter 报文中的 TTS 配置（注意：是直接替换，而不是将两者合并） |

## 返回报文格式

每个成功的 Task 持续返回多个数据包，分别为音频和音素包，各自按照逻辑顺序依次返回。如果在 Starter 请求中未要求返回音素，则仅返回一类。

返回报文的基本格式见下：

| 字段      | 名称          | 类型   | 是否必现 | 说明                                         |
| :-------- | :------------ | :----- | :------- | :------------------------------------------- |
| `session` | Session ID    | string | Yes      | 当前连接的 Session ID                        |
| `trace`   | Trace ID      | string | Yes      | 当前 Task 对应的 Trace ID                    |
| `status`  | Status Name   | enum   | Yes      | 当前 Task 的状态，正常为 `ok`，失败为 `fail` |
| `error`   | Error Message | string | No       | 如果失败，返回的错误信息                     |
| `tts`     | TTS Content   | map    | No       | 如果成功，返回的合成结果，具体字段含义见下   |

具体合成结果位于 TTS Content 中：

| 字段         | 名称                            | 类型   | 是否必现 | 说明                                                        |
| :----------- | :------------------------------ | :----- | :------- | :---------------------------------------------------------- |
| `id`         | Task ID                         | string | Yes      | 当前 Task 对应的 ID                                         |
| `index`      | Index No.                       | int    | Yes      | 返回音频包、音素包序列号                                    |
| `type`       | Package Type                    | enum   | Yes      | 音频包为 `audio`，音素包为 `phone`，表示全部发送完毕为`eof` |
| `audio_data` | Base64-encoded Audio Data       | string | No       | 音频数据，仅在音频包中有                                    |
| `phone_data` | Base64-encoded Phonetic Symbols | string | No       | 音素数据，仅在音素包中有                                    |

## 实际流程样例解析

### Case 1: 最小配置流程

Request: Starter

```json
{
  "type": "TTS3",
  "tts": {
    "phone": true
  }
}
```

Request: Task

```json
{
  "query": "大家好！"
}
```

Response: 1

```json
{
  "status": "ok",
  "session": "49d3af81-f344-4ccf-8231-574ceac1a260",
  "trace": "f2e13c02-c629-4db8-a942-4393583a5182",
  "tts": {
    "id": "4b69geebj4septyxh72qy885f",
    "index": 1,
    "type": "audio",
    "audio_data": "AAAAAAAAAAAAAAAAAAAAAAAAAAAAA...//wkA/P8CAP//AAD4/wMA0v8oAL3/...AAAAAAAAAAAAA=="
  }
}
```

Response: 2

```json
{
  "status": "ok",
  "session": "49d3af81-f344-4ccf-8231-574ceac1a260",
  "trace": "f2e13c02-c629-4db8-a942-4393583a5182",
  "tts": {
    "id": "4b69geebj4septyxh72qy885f",
    "index": 2,
    "type": "phone",
    "phone_data": "aiBqIGluIGluIGluIGluIGluIG...ZCBlbmQgZW5k"
  }
}
```

### Case 2: 完整配置流程

Request: Starter

```json
{
  "auth": "XSMLTGKQVVCPJCQHJZ4VEDMGIY",
  "type": "TTS3",
  "device": "device-wei",
  "session": "d6bad1c4-eda9-4abd-8294-7ebb4e72cf89",
  "tts": {
    "language": "zh-CN",
    "voice": "xiaoling",
    "speed_ratio": 1.05,
    "sample_rate": 16000,
    "volume": 200,
    "phone": true
  }
}
```

Request: Task

```json
{
  "id": "bf3qmpuuk18ktv7cv4b6kzhs9",
  "query": "大家好！",
  "ssml": false
}
```

Response: 1

```json
{
  "status": "ok",
  "session": "d6bad1c4-eda9-4abd-8294-7ebb4e72cf89",
  "trace": "335d508e-688f-4fc1-b057-4a4aa78b9ee7",
  "tts": {
    "id": "bf3qmpuuk18ktv7cv4b6kzhs9",
    "index": 1,
    "type": "audio",
    "audio_data": "AAAAAAAAAAAAAAAAAAAAAAAAAAAAA...//wkA/P8CAP//AAD4/wMA0v8oAL3/...AAAAAAAAAAAAA=="
  }
}
```

Response: 2

```json
{
  "status": "ok",
  "session": "d6bad1c4-eda9-4abd-8294-7ebb4e72cf89",
  "trace": "335d508e-688f-4fc1-b057-4a4aa78b9ee7",
  "tts": {
    "id": "bf3qmpuuk18ktv7cv4b6kzhs9",
    "index": 2,
    "type": "phone",
    "phone_data": "aiBqIGluIGluIGluIGluIGluIG...ZCBlbmQgZW5k"
  }
}
```
