---
title: "NLP 智能对话"
weight: 3
---

# Control System v1 - NLP2 - Developer Manual

中控 WebSocket 全双工接口第二 NLP 调用方式的说明，链接方式为 WebSocket 协议，报文为使用 UTF-8 编码的文本。

## 调用流程

1. 建立 WebSocket 连接（无需鉴权），如果在本地启动中控，地址通常为 `ws://localhost:8070/v1`；
2. 发送 Starter 包，内容为后续 NLP 请求的通用配置信息，如果格式错误或超过 10 秒未发送会被断开；
3. 发送 Query 文本包，内容为单条对话内容；
4. 收到对应的应答结果，格式为 JSON；
5. 如果 60 秒内没有收到任何请求，服务端会主动断开，建议以一定间隔发送 Ping 包进行保活；
6. 如果当前没有更多问答对话，可以直接断开（没有链接断开报文的设计）；

## 请求报文格式

### Starter

每次建立连接后发送的第一个包，表示此连接的目的和后续数据包的解析方式。格式为 JSON 文本，包含以下字段：

| 字段      | 名称          | 类型   | 默认值      | 说明                                                     |
| :-------- | :------------ | :----- | :---------- | :------------------------------------------------------- |
| `auth`    | AuthN Token   | string | 空字符串    | 设备鉴权 Token，如服务端开启鉴权则必填                   |
| `type`    | Workflow Type | string | 必填        | 此处填写`NLP2`                                           |
| `device`  | Device ID     | string | 空字符串    | 设备 ID，建议填写，以便追溯和定位问题                    |
| `session` | Session ID    | string | 随机 UUIDv4 | 建议调用者自行生成 Session ID 并填写，以便追溯和定位问题 |
| `nlp`     | NLP Config    | map    | 必填        | NLP 专属配置，具体信息见下                               |

NLP Config 配置见下：

| 字段         | 名称                           | 类型 | 默认值  | 说明                                         |
| :----------- | :----------------------------- | :--- | :------ | :------------------------------------------- |
| `omit_meta`  | Omit Meta Data in Response     | bool | `false` | 可选字段，是否删去语义附加信息，即默认会返回 |
| `omit_error` | Omit Error Message in Response | bool | `false` | 可选字段，是否删去报错信息，即默认会返回     |

### Query

Starter 包发送并成功建立连接后，后续可发送多个 Query 文本包提交用户问题，内容为单句纯文本。

## 返回报文格式

针对每一条成功处理的 Query ，都会有一条报文返回，当 `omit_error` 为 `false` 时，出错报文亦会返回。返回报文的基本格式见下：

| 字段      | 名称          | 类型   | 是否必现 | 说明                                       |
| :-------- | :------------ | :----- | :------- | :----------------------------------------- |
| `session` | Session ID    | string | Yes      | 当前连接的 Session ID                      |
| `trace`   | Trace ID      | string | Yes      | 当前句子对应的 Trace ID                    |
| `status`  | Status Name   | enum   | Yes      | 当前会话的状态，正常为 `ok`，失败为 `fail` |
| `error`   | Error Message | string | No       | 如果失败，返回的错误信息                   |
| `nlp`     | NLP Content   | map    | No       | 如果成功，返回的识别结果，具体字段含义见下 |

具体识别结果位于 NLP Content 中：

| 字段           | 名称                  | 类型   | 是否必现 | 说明                                                       |
| :------------- | :-------------------- | :----- | :------- | :--------------------------------------------------------- |
| `index`        | Index No.             | int    | Yes      | 返回包序列号                                               |
| `query`        | Query                 | string | Yes      | 提交的问题文本                                             |
| `answer`       | Answer                | string | Yes      | 返回的播报文本，数字人前端调用 TTS 进行播报                |
| `text`         | Text                  | string | No       | 返回的展示文本，数字人前端上屏作为文本展示                 |
| `image`        | Image                 | string | No       | 答案配套的图片资源 URL                                     |
| `event`        | Event                 | string | No       | 产生的相应事件的名称                                       |
| `meta`         | Meta Data             | map    | No       | 额外的语义附加信息，key-value 内嵌结构，视具体场景内容有变 |
| `preempt_mode` | Preempt Mode Type     | string | No       | 干预数字人前端的打断模式（详情见以下说明）                 |
| `delay_msec`   | Delay Interval (msec) | int    | No       | 干预数字人前端的播放延时毫秒数（详情见以下说明）           |

播报干预的两个字段 `preempt_mode` 和 `delay_msec` 的含义如下：

- `preempt_mode` 表示打断模式，有三种可选（取值为字符串类型）：

  - `none`：不打断，如果当前已经在播报，则忽略，否则进行播报；
  - `force`：强制打断，如果当前已经在播报，则打断并播报，否则进行播报；
  - `wait`: 等待，如果当前已经在播报，则等待播报完成后再播报，否则进行播报；
  - 取值为空或不指定，则按本地已有配置和实现来（通常为打断）。

- `delay_msec` 表示播放延时，即具备播放条件（即当前没有在播放，或者正在播放的内容被打断，或者等待正在播放的内容完成后）后的延时时间，单位为毫秒，取值为整数：
  - 取值为 0 或不指定，则没有延时；
  - 取值为负数，则没有延时；
  - 取值为正数，则延时为指定的毫秒数。

## 实际流程样例解析

### Case 1: 最小配置流程

Request: Starter

```json
{
  "type": "NLP2",
  "device": "xiaoyi",
  "nlp": {}
}
```

Request1: Query

```text
上海的天气
```

Response: 1

```json
{
  "status": "ok",
  "session": "4a38583a-82de-4044-a67d-50b9ee6db1e5",
  "trace": "461a79a3-9172-4c00-9df5-3739847f9a40",
  "nlp": {
    "index": 1,
    "query": "上海的天气",
    "answer": "上海今天的天气是晴转多云,温度范围是19到30摄氏度。",
    "text": "上海今天的天气是晴转多云,温度范围是19到30摄氏度。",
    "meta": {
      "audio_text": "上海今天的天气是晴转多云,温度范围是19到30摄氏度。",
      "confidence": 1,
      "device": "xiaoyi",
      "display_text": "上海今天的天气是晴转多云,温度范围是19到30摄氏度。",
      "intent": "weather",
      "skill": {
        "id": "__weather",
        "name": "__weather",
        "type": "system"
      },
      "vendor_name": "platform"
    }
  }
}
```

Request2: Query

```text
郭沫若的老婆是谁
```

Response: 2

```json
{
  "status": "ok",
  "session": "4a38583a-82de-4044-a67d-50b9ee6db1e5",
  "trace": "e2f63ed4-2871-4eee-87b6-ca201809d55c",
  "nlp": {
    "index": 2,
    "query": "郭沫若的老婆是谁",
    "answer": "陈赫老婆是张子萱",
    "text": "陈赫老婆是张子萱",
    "meta": {
      "audio_text": "陈赫老婆是张子萱",
      "confidence": 0.9847,
      "device": "xiaoyi",
      "display_text": "陈赫老婆是张子萱",
      "intent": "chitchat",
      "is_chitchat": true,
      "skill": {
        "id": "__chitchat",
        "name": "__chitchat",
        "type": "system"
      },
      "vendor_name": "platform"
    }
  }
}
```

Request3: Query

```text
聊会天
```

Response: 3

```json
{
  "status": "ok",
  "session": "4a38583a-82de-4044-a67d-50b9ee6db1e5",
  "trace": "3f2f84db-ea53-4a49-9114-7e3545080330",
  "nlp": {
    "index": 3,
    "query": "聊会天",
    "answer": "行呀，聊呗",
    "text": "行呀，聊呗",
    "meta": {
      "audio_text": "行呀，聊呗",
      "confidence": 0.9919,
      "device": "xiaoyi",
      "display_text": "行呀，聊呗",
      "intent": "chitchat",
      "is_chitchat": true,
      "skill": {
        "id": "__chitchat",
        "name": "__chitchat",
        "type": "system"
      },
      "vendor_name": "platform"
    }
  }
}
```

### Case 2: 完整配置流程

Request: Starter

```json
{
  "auth": "XSMLTGKQVVCPJCQHJZ4VEDMGIY",
  "type": "NLP2",
  "device": "xiaoyi",
  "nlp": {
    "omit_meta": true,
    "omit_error": true
  }
}
```

Request1: Query

```text
你会做些啥
```

Response: 1

```json
{
  "status": "ok",
  "session": "56c63016-c8c1-4486-9a6b-260826b6a6e4",
  "trace": "aaca9cdd-d9e3-493c-85cf-90e8535459b2",
  "nlp": {
    "index": 1,
    "query": "你会做些啥",
    "answer": "我是美貌和智慧并存的数字人, 我可是基于AI深度学习能力，通过对真人的大量会话表情和动作的学习，生成与人物惟妙惟肖的会话表情和动作。",
    "text": "我是美貌和智慧并存的数字人, 我可是基于AI深度学习能力，通过对真人的大量会话表情和动作的学习，生成与人物惟妙惟肖的会话表情和动作。"
  }
}
```

### Case 3: 错误情况的返回结果

1. 填写的 Workflow Type 不存在，WebSocket 会关闭并返回如下错误：

```text
websocket: close 1002 (protocol error): invalid nlp vendor id: 6
```

2. 填写的 AuthN Token 错误或为空，当服务端要求鉴权时，WebSocket 会关闭并返回如下错误：

```text
websocket: close 1008 (policy violation): invalid authentication token
```

3. `device` 漏填或为空字符串，且 `omit_error` 未设置为 `true`，则返回如下错误：

```json
{
  "status": "fail",
  "error": "http error status, code: 500, body: {\"error\":\"500 Internal Server Error\"}\n",
  "session": "b3c451cc-edf7-41f8-b375-08ffc5b16daf",
  "trace": "ae810786-3eca-46e6-b839-af0950cd750b"
}
```

如已设置为 `true`，则不返回。
