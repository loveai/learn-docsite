---
title: "ASR 语音识别"
weight: 1
---

# Control System v1 - ASR5 - Developer Manual

中控 WebSocket 全双工接口第五 ASR 调用方式的说明，链接方式为 WebSocket 协议，控制报文为使用 UTF-8 编码的 JSON 文本。

## 调用流程

1. 建立 WebSocket 连接（无需鉴权），如果在本地启动中控，地址通常为 `ws://localhost:8070/v1`；
2. 发送 Starter 包，内容为后续 ASR 请求的通用配置信息，如果格式错误或超过 10 秒未发送会被断开；
3. 发送 Data 二进制数据包，内容为 PCM 音频；
4. 音频发送完成后，发送 EOF 包；（可选，不发送则需额外发送 500 毫秒以上的环境静音，帮助 VAD 结束）
5. 收到对应识别结果，格式为 JSON 文本；3、4 步可重复
6. 如果 60 秒内没有收到任何请求，服务端会主动断开，建议以一定间隔发送 Ping 包进行保活；
7. 如果当前没有更多语音识别任务，可以直接断开（没有链接断开报文的设计）；

## 请求报文格式

### Starter

每次建立连接后发送的第一个包，表示此连接的目的和后续数据包的解析方式。格式为 JSON 文本，包含以下字段：

| 字段      | 名称          | 类型   | 默认值      | 说明                                                     |
| :-------- | :------------ | :----- | :---------- | :------------------------------------------------------- |
| `auth`    | AuthN Token   | string | 空字符串    | 设备鉴权 Token，如服务端开启鉴权则必填                   |
| `type`    | Workflow Type | string | 必填        | 此处填写`ASR5`                                           |
| `device`  | Device ID     | string | 空字符串    | 设备 ID，建议填写，以便追溯和定位问题                    |
| `session` | Session ID    | string | 随机 UUIDv4 | 建议调用者自行生成 Session ID 并填写，以便追溯和定位问题 |
| `asr`     | ASR Config    | map    | 必填        | ASR 专属配置，具体信息见下                               |

ASR Config 配置见下：

| 字段         | 名称              | 类型  | 默认值 | 说明                                                       |
| :----------- | :---------------- | :---- | :----- | :--------------------------------------------------------- |
| `mic_volume` | Microphone Volume | float | `1.0`  | 可选字段，麦克风音量用于 ASR 进行自增益，支持范围为 0 到 1 |

### Data

Starter 包发送并成功建立连接后，后续可重复发送多个二进制 Data 包流式提交音频。

输入的音频流格式为 PCM，使用 16KHz 采样率，16bit 数据位宽，单通道，小端。即 `sox -t raw -r 16000 -e signed -b 16 -c 1` 可转格式，或 `ffmpeg -acodec pcm_s16le -ac 1 -ar 16000` 可转格式。

按音频从麦克风读取的速率发送，建议为每 40 毫秒发送 1280 字节，或每 160 毫秒发送 5120 字节。

### EOF

音频包发送完成后，发送 EOF 包，表示结束识别。格式为 JSON 文本，如下：

```json
{
  "signal": "eof"
}
```

## 返回报文格式

每句被完整识别的文字都会返回一条报文，中间识别结果不返回。

返回报文的基本格式见下：

| 字段      | 名称          | 类型   | 是否必现 | 说明                                       |
| :-------- | :------------ | :----- | :------- | :----------------------------------------- |
| `session` | Session ID    | string | Yes      | 当前连接的 Session ID                      |
| `trace`   | Trace ID      | string | Yes      | 当前句子对应的 Trace ID                    |
| `status`  | Status Name   | enum   | Yes      | 当前会话的状态，正常为 `ok`，失败为 `fail` |
| `error`   | Error Message | string | No       | 如果失败，返回的错误信息                   |
| `asr`     | ASR Content   | map    | No       | 如果成功，返回的识别结果，具体字段含义见下 |

具体识别结果位于 ASR Content 中：

| 字段    | 名称      | 类型   | 是否必现 | 说明         |
| :------ | :-------- | :----- | :------- | :----------- |
| `index` | Index No. | int    | Yes      | 返回包序列号 |
| `text`  | Text      | string | Yes      | 文字识别结果 |

## 实际流程样例解析

### Case 1: 最小配置流程

Request: Starter

```json
{
  "type": "ASR5",
  "asr": {}
}
```

Request: 二进制 Data 略

Response: 1

```json
{
  "session": "4ea613f2-b1d4-47cf-8033-db59aae66721",
  "trace": "e1c44bdc-4f9a-487c-806e-005679db7d0d",
  "asr": {
    "index": 1,
    "text": "早知道你喜欢十里春光"
  }
}
```

Response: 2

```json
{
  "session": "4ea613f2-b1d4-47cf-8033-db59aae66721",
  "trace": "f7551818-5025-4d83-b41b-136bb19b5b5f",
  "asr": {
    "index": 2,
    "text": "我一定会在麦田里种满玫瑰和山茶"
  }
}
```

Response: 3

```json
{
  "session": "4ea613f2-b1d4-47cf-8033-db59aae66721",
  "trace": "89d3a8b4-a291-4cdc-9b78-d3f912d06223",
  "asr": {
    "index": 3,
    "text": "你路过这片土地才算浪漫"
  }
}
```

### Case 2: 完整配置流程

Request: Starter

```json
{
  "auth": "XSMLTGKQVVCPJCQHJZ4VEDMGIY",
  "type": "ASR5",
  "session": "8f97055c-bd29-41c7-92d1-3933fed566fa",
  "asr": {
    "mic_volume": 0.67
  }
}
```

Request: 二进制 Data 略

Request: EOF

```json
{
  "signal": "eof"
}
```

Response: 1

```json
{
  "session": "8f97055c-bd29-41c7-92d1-3933fed566fa",
  "trace": "52517513-875a-47b6-bd30-f11a75e26745",
  "asr": {
    "index": 1,
    "text": "介绍一下长宁图书馆"
  }
}
```

Response: 2

```json
{
  "session": "8f97055c-bd29-41c7-92d1-3933fed566fa",
  "trace": "74347673-0fd5-4712-aca6-78a052bc84be",
  "asr": {
    "index": 2,
    "text": "长宁图书馆的开放时间"
  }
}
```

### Case 3: 错误情况的返回结果

1. 填写的 Workflow Type 不存在，WebSocket 会关闭并返回如下错误：

```text
websocket: close 1002 (protocol error): invalid asr vendor id: 7
```

2. 填写的 AuthN Token 错误或为空，当服务端要求鉴权时，WebSocket 会关闭并返回如下错误：

```text
websocket: close 1008 (policy violation): invalid authentication token
```

3. 并发超过限制，WebSocket 不会返回识别内容，内部会记录错误。
