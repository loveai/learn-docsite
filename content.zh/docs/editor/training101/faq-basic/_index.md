---
weight: 7
bookToc: true
title: "常见问题解答"
---

# 常见问题解答

这个回答了一些常见的与编辑相关的问题。

<!--more-->

## 1. 如何插入内部链接

方法一，可以用 `ref` 写从 **content** 目录开始的相对路径，例如：参考[基础知识]({{< ref "/content.zh/docs/editor/training101/concept/_index.md" >}})。

```tpl
参考[基础知识]({{</* ref "/content.zh/docs/editor/training101/concept/_index.md" */>}})
```

方法二，可以用 Markdown 语法写当前目录开始的子目录相对路径，例如：

```markdown
内容较长，分为了[本地环境的安装配置](setup/_index.md)和[编辑、预览与推送](preview/_index.md)两部分。
```

对于方法二，如果目标是 **_index.md** 文件，也可以略去不写：

```markdown
内容较长，分为了[本地环境的安装配置](setup)和[编辑、预览与推送](preview)两部分。
```
## 2. 如何插入图片

可以放在当前文档同一级目录下，也可以放在同一级目录的 `images` 目录下。相对路径的计算要考虑两种情况：

1. 如果是从 **_index.md** 文件引用，可以直接写相对路径，例如：

```markdown
![示意图](images/overview.png)
```

2. 如果是从其他文件引用，需要考虑相对路径增加的层级，例如：

```markdown
![示意图](../images/overview.png)
```

## 3. 如何插入示意图

推荐使用 Mermaid.js，详细资料参考[**基础知识部分**]({{< ref "/content.zh/docs/editor/training101/concept/_index.md#4-mermaidjs-%e5%88%b6%e5%9b%be%e5%8f%82%e8%80%83" >}})，使用方式参考[**组件使用部分**]({{< ref "/content.zh/docs/editor/training101/config/_index.md#37-%E7%A4%BA%E6%84%8F%E5%9B%BE-mermaidjs" >}})。

## 4. 如何插入音频

文件路径类似同图片，使用 HTML 语法插入即可，考虑到浏览器的兼容性推荐使用 mp3 格式：

<audio controls>
  <source src="audio.wav" type="audio/x-wav">
  Your browser does not support the audio tag.
</audio>

```html
<audio controls>
  <source src="audio.wav" type="audio/x-wav">
  Your browser does not support the audio tag.
</audio>
```

## 5. 如何插入视频

同样使用 HTML 语法插入即可，视频文件强烈建议放置在 CDN 上，节省流量费用，也避免文档站因为体积庞大而发布缓慢：

<video controls="controls" style="width: 100%;" autoplay="autoplay" loop="loop">
    <source src="https://cdn2.unrealengine.com/homepage-opener-5a55a50aaa4c.mp4" type="video/mp4" />
</video>

```html
<video controls="controls" style="width: 100%;" autoplay="autoplay" loop="loop">
    <source src="https://cdn2.unrealengine.com/homepage-opener-5a55a50aaa4c.mp4" type="video/mp4" />
</video>
```

## 6. 如何插入动图

可以使用 ffmpeg 将视频转换为高质量 gif 图片，然后遵循图片插入方式即可。具体操作，参考[此 Bash 脚本](https://github.com/an63/video2gif)项目。

## 7. 如何插入表格

使用 Markdown 语法即可，如果有些合并单元格的特殊样式，可以使用 HTML 语法。

## 8. 如何插入代码内容

可以使用 Markdown 的语法，并获得一些扩展。

例如，此例子表示以 HTML 语法高亮展示代码并显示行号，初始行号从100开始，并把相对的第2行和4到6行标记：

{{< highlight markdown >}}
```html{linenos=table,hl_lines=[2,"4-6"],linenostart=100}
<section id="main">
  <div>
   <h1 id="title">{{ .Title }}</h1>
    {{ range .Pages }}
        {{ .Render "summary"}}
    {{ end }}
  </div>
</section>
```
{{< /highlight >}}

```html{linenos=table,hl_lines=[2,"4-6"],linenostart=100}
<section id="main">
  <div>
   <h1 id="title">{{ .Title }}</h1>
    {{ range .Pages }}
        {{ .Render "summary"}}
    {{ end }}
  </div>
</section>
```

除去在 Markdown 的语法上做扩展，使用 Hugo 自带的语法高亮组件 **highlight** 也可以获得此效果，具体参考[官方文档](https://gohugo.io/content-management/syntax-highlighting/)。

## 9. 如何插入代码演示

在静态的代码按行展示之外，如果希望插入一段命令行的交互式演示，推荐使用 [asciinema](https://asciinema.org/)，具体操作参考[工具的文档](https://asciinema.org/docs/embedding)。

这是一个嵌入的例子：

```html
<script id="asciicast-m8KsjNReIMup9WJwbdjhL3zoi" src="https://asciinema.org/a/m8KsjNReIMup9WJwbdjhL3zoi.js" async></script>
```

<script id="asciicast-m8KsjNReIMup9WJwbdjhL3zoi" src="https://asciinema.org/a/m8KsjNReIMup9WJwbdjhL3zoi.js" async></script>
