---
weight: 2
bookToc: true
title: "本地编辑和预览"
---

# 本地编辑和预览

这部分将介绍如何在本地编辑和预览文档，并使用命令行工具进行推送，并在 GitLab 上进行后续操作。

## 1. 本地拉取代码

首先，我们需要在本地拉取代码，这里我们使用 GitLab 项目页面上找到对应的 Clone 地址，例如：

```
git@gitlab.com:loveai/learn-docsite.git
```

然后我们打开对应的终端，根据 Clone 地址收入以下命令进行拉取（第一次操作需要按提示输入 `yes` 确认）：

```bash
git clone git@gitlab.com:loveai/learn-docsite.git
```

拉取成功后，我们可以看到当前目录下多了一个 `learn-docsite` 目录，这个目录就是我们的项目目录。可以用 Visual Studio Code 打开这个目录，然后我们就可以在本地进行编辑操作。

## 2. 本地编辑

在本地编辑时，我们可以使用 Visual Studio Code 进行编辑，也可以使用其他编辑器进行编辑。这部分主要要符合 Markdown 语法及 Hugo 站和主题的相关语法规范。如果出现了错误，可以在本地预览时进行查看到错误信息或样式。

## 3. 本地预览

继续在终端中输入以下命令，进行本地预览：

```bash
cd learn-docsite
make dev
```

等几秒钟刷屏结束，保持终端窗口不要关闭，我们就可以根据提示在浏览器中打开对应的本地页面进行预览了。此处是打开 http://localhost:1313/learn-docsite/ 进行预览了。

这时候可以回到 Visual Studio Code 继续编辑，保存后，浏览器中的页面会自动刷新，我们就可以看到编辑后的效果了。

![VS Code Preview](vscode-preview.png)

## 4. 命令行提交

如果你对你的修改已经满意，打算提交到 GitLab 上，可以在 Visual Studio Code 的终端中依次输入输入命令进行提交：

创建 `edit1` 分支（名字记得自己取哦）：

```bash
git checkout -b edit1
```

纳入当前全部文件：

```bash
git add -A
```

确认当前工作区状态：

```bash
git status
```

创建消息为 `Aloha Hugo` 的提交（消息内容记得自己写有意义的）：

```bash
git commit -a -m "Aloha Hugo"
```

将分支推送到 GitLab 上：

```bash
git push origin
```

注意这一步的输出，会有一个链接，我们可以点击这个链接，进入到 GitLab 的 Merge Request 页面，创建合并请求（如下方例子所示）：

```text
PS C:\Users\deskuser\Documents\learn-docsite> git push origin
>>
Enumerating objects: 7, done.
Counting objects: 100% (7/7), done.
Delta compression using up to 4 threads
Compressing objects: 100% (4/4), done.
Writing objects: 100% (4/4), 425 bytes | 212.00 KiB/s, done.
Total 4 (delta 1), reused 0 (delta 0), pack-reused 0
remote: 
remote: To create a merge request for edit1, visit:
remote:   https://gitlab.com/loveai/learn-docsite/-/merge_requests/new?merge_request%5Bsource_branch%5D=edit1
remote:
To gitlab.com:loveai/learn-docsite.git
 * [new branch]      edit1 -> edit1
branch 'edit1' set up to track 'origin/edit1'.
```

这一步的 Merge Request 创建成功后，我们就可以在 GitLab 上看到这个 Merge Request 了，然后就可以等待预览和进一步审核了。具体后续步骤与前一讲 “在线快速编辑” 中描述的一致。
