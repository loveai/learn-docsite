---
weight: 1
bookToc: true
title: "环境安装和配置"
---

# 环境安装和配置

这部分将介绍如何在 Windows 和 macOS 系统中安装必要的软件以及进行配置。

## 1. Visual Studio Code 安装和插件

打开官方网站 https://code.visualstudio.com/ 下载安装包并进行安装。安装过程中，使用 Windows 的同学记得勾选 ☑️ “Open with Code” 的选项（下图红框所示），方便日后使用 VSCode 直接打开文件文件。

![Open with Code](vscode-open-with.png)

{{< columns >}}

安装完成后，打开 VSCode，点击中部的扩展图标，搜索并安装以下插件：

```text
markdownlint
Markdown Preview Enhanced
Markdown All in One
```

具体操作和选择参见右图的黄色线框的标记。

当你条件允许的情况下，也推荐你安装 [GitHub Copilot](https://github.com/features/copilot) 付费插件，这是 GitHub 推出的一个 AI 代码补全插件，同样可以帮助你提高文档编写效率。

<--->

![VSCode Extensions](vscode-extension.png)

{{< /columns >}}

## 2. 包管理软件安装

为了便于安装后续的开发类软件，我们推荐使用包管理软件进行安装，两个操作系统需要选择不同的包管理软件。

{{< tabs "package" >}}

{{< tab "macOS" >}}

对于 macOS 系统，我们推荐使用 Homebrew 进行软件管理。

Homebrew 的官方网站是 https://brew.sh/ ，打开网站后，按照网站上的说明进行安装即可。即便你已经安装过 Homebrew，也建议你重新安装一次，以确保你的 Homebrew 是最新版本。

即，在公司内网环境或可以科学上网的环境中，打开终端（Terminal 或 iTerm2），完整输入以下命令进行安装：

```bash
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
```

安装完成后，输入以下命令，确认 Homebrew 已经安装成功：

```bash
brew -v
```

安装正确的话，会显示版本号，否则会显示命令不存在。

{{< /tab >}}

{{< tab "Windows" >}}

对于 Windows 系统，我们推荐使用 Chocolatey 进行软件管理。

Chocolatey 的官方网站是 https://chocolatey.org/ ，打开网站后，按照[网站上的说明](https://chocolatey.org/install)进行安装即可。

即，在公司内网环境或可以科学上网的环境中，打开具有管理员权限的 PowerShell 终端窗口——

- 对于 Windows 10 系统，打开 Explorer 选择任意目录，在左上角 File 菜单中选择 **Open Windows PowerShell** 中的 **Open Windows PowerShell as Administrator** 选项；
- 对于 Windows 11 系统，打开 Explorer 选择任意目录，右击选择 **Open in Windows Terminal** 选项；

完整输入以下命令进行安装：

```ps1
Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))
```

提示安装成功后，关闭 PowerShell，按以上方法再次打开，输入以下命令，确认 Chocolatey 已经安装成功：

```ps1
choco
```

安装正确的话，会显示版本号，否则会显示命令不存在。

![choco install](choco-install.png)

{{< /tab >}}

{{< /tabs >}}

## 3. Hugo、Makefile 安装

借助前一步安装的包管理软件，我们可以很方便地安装 Hugo 和 Makefile。

{{< tabs "make-hugo" >}}

{{< tab "macOS" >}}

对于 macOS 系统，在公司内网环境或可以科学上网的环境中，打开终端（Terminal 或 iTerm2）完整输入以下命令进行安装：

安装 Makefile 等基础开发工具：

```bash
xcode-select --install
```

安装 Hugo：

```bash
brew install hugo
```

{{< /tab >}}

{{< tab "Windows" >}}

对于 Windows 系统，在公司内网环境或可以科学上网的环境中，打开具有管理员权限的 PowerShell 终端窗口——

安装 Makefile 工具：

```ps1
choco install -y make
```

安装 Hugo：

```ps1
choco install -y hugo-extended
```

{{< /tab >}}

{{< /tabs >}}

安装完成后，依次输入以下命令，确认 Makefile 和 Hugo 已经安装成功：

```bash
make --version
hugo version
```

一个可能的结果如下：

```text
PS C:\> make --version
GNU Make 4.3
Built for Windows32
Copyright (C) 1988-2020 Free Software Foundation, Inc.
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
PS C:\> hugo version
hugo v0.110.0-e32a493b7826d02763c3b79623952e625402b168+extended windows/amd64 BuildDate=2023-01-17T12:16:09Z VendorInfo=gohugoio
PS C:\>
```

## 4. Git 安装和配置

最后，我们需要安装 Git 并进行必要的配置。

{{< tabs "git-config" >}}

{{< tab "macOS" >}}

对于 macOS 系统，在公司内网环境或可以科学上网的环境中，打开终端（Terminal 或 iTerm2）完整输入以下命令安装 Git：

```bash
brew install git
```

{{< /tab >}}

{{< tab "Windows" >}}

对于 Windows 系统，在公司内网环境或可以科学上网的环境中，打开具有管理员权限的 PowerShell 终端窗口，完整输入以下命令安装 Git：

```ps1
choco install -y git.install
```

{{< /tab >}}

{{< /tabs >}}


安装完成后，输入以下命令获取版本号，确认安装成功：

```bash
git version
```

{{< hint warning >}}
**如果在 PowerShell 中无法找到 Git**

对于 Windows 来说，通过 Chocolatey 安装的 Git 可能无法在 PowerShell 中被找到，有三种方式可以解决：

1. 需要手动将 Git 的安装路径添加到系统环境变量中；
2. 使用 Git Bash 作为终端窗口；
3. 从[官方网站](https://git-scm.com/)下载对应安装包进行安装，例如 [Git-2.39.1-64-bit.exe](https://github.com/git-for-windows/git/releases/download/v2.39.1.windows.1/Git-2.39.1-64-bit.exe)；

{{< /hint >}}

确认安装成功后，我们需要进行一些必要的配置：

设置全局的用户名和邮箱：

```bash
git config --global user.name "Your Name"
git config --global user.email "alias@company.com"
```

设置的相关推送配置：

```bash
git config --global --add push.default current
git config --global --add push.autoSetupRemote true
git config --global push.default simple
git config --global pull.rebase true
git config --global help.autocorrect 1
```

生成 SSH 密钥：

```bash
ssh-keygen -t rsa -b 2048
```

一路回车，直到生成 SSH 密钥成功，用 `cat` 命令获取公钥内容：

```bash
cat ~/.ssh/id_rsa.pub
```

然后将此公钥添加到 GitLab 中：点击右上角头像，在菜单中选择 **Preferences**，在打开的设置页面的左侧菜单中选择 **SSH Keys**，在右侧页面 **Add SSH Key** 部分，将公钥内容完整的粘贴到 **Key** 输入框中，失效日期 **Expiration date** 可以点击清除， 点击 **Add Key** 按钮即可。
