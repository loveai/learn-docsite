---
weight: 1
bookFlatSection: false
bookCollapseSection: true
bookToc: false
title: "文档开发培训101"
---

# 文档开发培训 101 - 2023 年 2 月

基于这套 Hugo+Markdown+GitLab CI 的文档网站，我们将在 2023 年 2 月举办一次文档开发培训，希望能够帮助大家更好地使用这套文档网站。我将依次介绍一下 Markdown 文档的编辑方式，Hugo 文档站编辑、整理要领，以及基于 GitLab 的工作流。

## 内容提纲

- 现有站点体验
- 基础知识：
  - 静态站概念
  - Hugo 站点目录结构
  - Markdown 语法（参考资料）
  - Mermaid 图表（附赠图书）
  - Git 基本操作（概念和按步骤介绍）
  - GitLab 工作流
- 快速开始：
  - GitLab 在线编辑
  - 预览效果
  - 完成发布流程
- 本地编辑环境：
  - 软件安装：Hugo、Git、VSCode、Makefile
  - 本地预览方法
  - 本地编辑
  - 推送 GitLab 发布
- 站点调整技巧：
  - 站点基本样式调整
  - 目录结构调整
  - 多语言内容支持
  - 组件使用介绍
- 常见问题解答：
  - 内部链接如何做
  - 如何插入示意图
  - 如何插入图片
  - 如何插入音频
  - 如何插入视频
  - 如何插入动图
  - 如何插入表格
  - 如何插入代码内容
  - 如何插入代码演示
- 进阶问题解答：
  - 如何修改网站样式：当前主题，换主题
  - 如何内部发布
  - 如何删改并公开发布
  - 如何打包给客户
- 更多内容：
  - 文档站参考：MSDN、Sentry、百度 AI

## 内容预览

内容分为七讲，每一讲的介绍如下：

{{<section>}}
