---
weight: 3
bookToc: true
title: "基础知识"
---

# 基础知识和概念

这部分内容主要介绍我们使用的相关技术和参考文档，并不是期望大家都能完全掌握，只是为了让大家有一个基本的认识，以便在后续的工作中能够更好的理解和使用。

<!--more-->

## 1. 静态站概念

基于 JAMStack 架构，类比于传统的 LAMP 架构：

- LAMP: Linux, Apache, MySQL, PHP/Perl/Python 依赖于服务器端的动态语言进行构建
- JAMStack: JavaScript, APIs, Markup 依赖于客户端的静态语言进行构建

回归 1990 年代初代互联网的静态资源的分发方式，但是在这个时代，我们可以使用 JavaScript 来实现更多的交互功能，而不是依赖于服务器端的动态语言。

技术选型主要依赖于：

- 静态网站生成器：我们选择 [Hugo](https://gohugo.io/)，因为使用广泛、构建足够快，其他[生成器参考](https://jamstack.org/generators/)；
- 样式主题：根据我们对文档站功能的需求，最终选择 [Hugo Book](https://github.com/alex-shpak/hugo-book)；

## 2. 站点目录结构

整个站点依赖的文件夹结构如下，因为受到 Hugo 框架的影响，初步看起来比较多，但是大家需要修改的很少——其中 `content.zh` 和 `content.en` 分别是中文和英文的文档内容，`config.dev.toml` 和 `config.prod.toml` 分别是内部测试站和公开正式站的配置文件，`static` 是静态资源目录可以放图片，`public` 是最终输出网页结果的目录。

```text
❤
├── config.dev.toml   # 内部测试站配置
├── config.prod.toml  # 公开正式站配置
├── archetypes
├── assets
├── content.en    # ✍️ 英文文档内容
│   ├── docs
├── content.zh    # ✍️ 中文文档内容
│   └── docs
├── data
├── layouts
├── public        # 📦 最终输出路径
├── resources
├── static        # 🖼️ 静态资源
│   └── images
└── themes        # 主题目录
    └── hugo-book
```

## 3. Markdown 语法参考

一句话讲明他们之间的关系 —— Markdown 是一种略微松散的标记语言，Hugo 框架选用了 goldmark 来解析基于 CommonMark 0.30 规范的 Markdown 语法，因此我们可以参考 CommonMark 规范来编写文档。

常见并被广泛使用的的 GitHub 方言（GitHub Flavored Markdown）也是基于 CommonMark 规范的，也有一定的参考意义。

- goldmark 解析器参考: https://github.com/yuin/goldmark/
- CommonMark 规范参考: https://spec.commonmark.org/0.30/
- GitHub 方言参考: https://github.github.com/gfm/

如果你对 Markdown 语法不熟悉，可以参考下面的资料：

- 一分钟快速语法参考: https://commonmark.org/help/
- 十分钟入门互动课程: https://commonmark.org/help/tutorial/
- 基本语法参考: https://www.markdownguide.org/basic-syntax/

## 4. Mermaid.js 制图参考

文档站所集成的 Mermaid.js 是一个基于 JavaScript 的图表库，可以用来绘制流程图、时序图、甘特图、类图、状态图、甚至是 UML 图表，和 Markdown 一样可以通过简单的纯文本就可以进行编辑操作，不同于类似 Microsoft Word 或 Visio 之类严重依赖鼠标操作的工具。

- 官方网站: https://mermaid.js.org/
- 各类图表语法参考: https://mermaid.js.org/syntax/flowchart.html
- 在线编辑器: https://mermaid.live/
- 源代码和简单示例: https://github.com/mermaid-js/mermaid#examples
- 更多复杂示例: https://mermaid.js.org/syntax/examples.html

{{< columns >}}

{{< mermaid >}}
graph TD
  A[开始]-->B
  B{这个流程图是死循环}-->|是的|B
  C[结束]
{{< /mermaid >}}

<--->

```tpl
{{</*/* mermaid */*/>}}
graph TD
  A[开始]-->B
  B{这个流程图是死循环}-->|是的|B
  C[结束]
{{</*/* /mermaid */*/>}}
```

{{< /columns >}}

{{< columns >}}

{{< mermaid >}}
pie title "常常念叨“爷青回”的人"
  "90 后" : 80
  "00 后" : 15
  "老年人" : 5
{{< /mermaid >}}

<--->

```tpl
{{</*/* mermaid */*/>}}
pie title 常常念叨“爷青回”的人
  "90后" : 80
  "00后" : 15
  "老年人" : 5
{{</*/* /mermaid */*/>}}
```

{{< /columns >}}

## 5. Git 基本操作

为了方便大家参与文档的编写，我们提供了一个 GitLab 仓库，你可以通过 GitLab 的 Web 界面来进行文档的编辑操作，也可以通过 Git 命令行工具来进行操作。

大家需要掌握的 Git 基本操作有：

- 克隆仓库 Clone
- 拉取最新代码 Pull
- 创建分支 Branch
- 提交代码 Commit
- 推送代码 Push
- 创建合并请求 Merge Request

这些操作我会结合编辑场景，用 Web 界面和命令行各演示一遍，大家如果不太熟悉 Git，可以先看看演示或者录播，然后再自己动手操作一遍。

关于 Git 的进一步学习，推荐参考如下资料：

- Git 交互式教程: https://learngitbranching.js.org/?locale=zh_CN
- 中文的非官方 Git 教程: https://www.runoob.com/git/git-tutorial.html
- 来自 Pluralsight 的英文视频课程：
  - Git Real: https://www.pluralsight.com/courses/code-school-git-real
  - Git Real 2: https://www.pluralsight.com/courses/code-school-git-real-2
  - GitLab Fundamentals: https://app.pluralsight.com/library/courses/gitlab-fundamentals/table-of-contents
