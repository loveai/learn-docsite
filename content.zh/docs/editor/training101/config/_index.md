---
weight: 6
bookToc: true
title: "站点配置调整"
---

# 站点配置调整

本章节将介绍如何调整站点和文章的配置，以及如何使用内置组件展现更丰富的文档效果。

<!--more-->

## 1. 配置调整

常见有三个方式进行调整，它们适用于不同的场景和目的：

- **config.toml** 配置文件 - 主要处理整站的配置，如站点名称、图表、主题、语言等；
- **content** 文件夹 - 主要包含文章的内容，具体的文件夹结构和文件名调整会导致站点目录、菜单结构发生变化；
- 每篇文档首部的 **front matter** - 主要处理文章的属性，如标题、自身目录、目录菜单中排序等；

具体配置要点如下：

### 1.1. 站点基本样式调整

为了区分具体环境，我们准备了两套 **config.toml** 配置文件分别对应 **dev** 和 **prod** 环境，它们结构相当，可以通过 `hugo --config` 参数进行指定。在 **Makefile** 中也分别做了 `make dev` 和 `make prod` 两个命令方便本地预览。

配置文件中的内容比较多，我们具体需要关注和调整的有——

- `baseURL` - 站点根地址，在部署到不同环境时需要调整后再打包；
- `disableLanguages` - 不显示的多语言，如果某个语言文档和配置存在，但是不希望展示，可以写在这里；
- `ignoreFiles` - 隐藏的目录，如果某个目录下的文档不希望展示，可以写在这里进行过滤；
- `languages` - 多语言配置：
  - `title` - 站点名称，会显示在左上角；
  - `languageName` - 语言名称，会显示在语言菜单中；
  - `weight` - 在语言菜单中的排序，从1开始的整数，数字越小越靠前；
  - `contentDir` - 文档目录，指向对应语言的文档目录；
- `menu.after` - 底部菜单配置，支持多语言配置，可以作为“友情链接”的类似用途；
- `param` - 一些和主题相关的独特配置，可以参考主题文档：
  - `BookTheme` - 是否启用夜间主题模式，`auto` 表示跟随用户操作系统配置，`light` 为始终白天模式，`dark` 为始终夜间模式；
  - `BookRepo` 和 `BookEditPath` - 控制文章右下角“编辑本页”跳转的代码仓链接，都为空则不显示此链接；

### 1.2. 目录结构调整

每个 **content** 文件夹会对应某个语音的全部文档。

我们当前站点的文件夹结构如下，以此为例：

- `content.zh` 文件夹下的根目录中，`_index.md` 对应是站点首页，即第一次进入时的默认页面，`docs` 文件夹下是具体的文档内容；
- 内部的每一级目录都准寻相同准则进行递归，即 `_index.md` 是该菜单目录的首页，其他的同级目录或者 `.md` 文件都是该菜单目录下的子页；
- 目录中只有含有有效正文内容的 `.md` 文件才会被解析为文档页面。即，空 `.md` 文件或者只有 **front matter** 的 `.md` 文件不会被解析，无法显示在菜单目录上；

```text
content.zh
├── _index.md
└── docs
    ├── develop
    │   ├── _index.md
    │   └── chat
    │       ├── ASR5.md
    │       ├── NLP2.md
    │       ├── TTS3.md
    │       └── _index.md
    └── editor
        ├── _index.md
        ├── ok.png
        └── training101
            ├── _index.md
            ├── concept
            │   └── _index.md
            ├── config
            │   └── _index.md
            ├── edit-local
            │   ├── _index.md
            │   ├── preview
            │   │   ├── _index.md
            │   │   └── vscode-preview.png
            │   └── setup
            │       ├── _index.md
            │       ├── choco-install.png
            │       ├── vscode-extension.png
            │       └── vscode-open-with.png
            ├── edit-quick
            │   └── _index.md
            ├── experience
            │   └── _index.md
            ├── faq-advanced
            │   └── _index.md
            └── faq-basic
                └── _index.md
```

### 1.3. 菜单文档调整

在 **content** 文件夹下的每个 `.md` 文件都是一个文档页面，它的 **front matter** 中可以配置一些属性，用于控制文档和菜单的显示效果。一个典型的 **front matter** 如下：

```yaml
---
title: "✍️ 文档编写指南"
weight: 3
bookHidden: false
bookFlatSection: true
bookToc: false
---
```

各字段的具体能力和作用如下：

- `title` - 文档标题，会显示在菜单目录上；
- `weight` - 文档在菜单目录中的排序权重，与同级进行比较，从1开始的整数，数字越小越靠前；
- `bookToc` - 是否在文档页面右侧显示内容的目录，`true` 为显示（默认），`false` 为不显示；
- `bookFlatSection` - 在菜单目录中的显示样式，`true` 为平铺显示，`false` 为文件树嵌套显示（默认）；
- `bookCollapseSection` - 当嵌套显示时，是否默认折叠该目录，`true` 为折叠，`false` 为不折叠（默认）；
- `bookHidden` - 是否在菜单目录中隐藏该文档，`true` 为隐藏，`false` 为显示（默认）；
- `bookSearchExclude` - 是否在搜索中排除该文档，`true` 为排除，`false` 为不排除（默认）；

## 2. 多语言支持

多语言支持是通过 Hugo 的 **多站点** 功能实现的，具体的配置方法可以参考 Hugo 官方文档：[Multi-language Mode](https://gohugo.io/content-management/multilingual/)。

在我们当前的配置下，不同语言的内容会被分别放置在 `content.` 开头命名的内容文件夹下，每个文件夹下的目录结构和文件内容推荐保持一样的，只是文档内容的语言不同。

## 3. 组件使用

除去 Markdown 自带的基本组件外，一些常用的组件可以通过 Hugo 的 **Shortcodes** 功能来实现，具体能够使用的 **Shortcodes** 和主题的实现还有关系。当前主题下，我们能使用的组件和样例如下：

### 3.1. 按钮 Buttons

一个按钮组件，用于标记特别的链接。这个链接点击后会在新页面打开，而不是当前页面。

```tpl
{{</* button relref="/" */>}}Get Home{{</* /button */>}}
{{</* button href="https://www.google.com/" */>}}Google{{</* /button */>}}
```

{{< button relref="/" >}}Get Home{{< /button >}}
{{< button href="https://www.google.com/" >}}Google{{< /button >}}

### 3.2. 分栏 Columns

可以将内容分为多栏显示。

```html
{{</* columns */>}} <!-- begin columns block -->
# Left Content
Lorem markdownum insigne...

<---> <!-- magic separator, between columns -->

# Mid Content
Lorem markdownum insigne...

<---> <!-- magic separator, between columns -->

# Right Content
Lorem markdownum insigne...
{{</* /columns */>}}
```

{{< columns >}}
## Left Content
Lorem markdownum insigne. Olympo signis Delphis! Retexi Nereius nova develat
stringit, frustra Saturnius uteroque inter! Oculis non ritibus Telethusa
protulit, sed sed aere valvis inhaesuro Pallas animam: qui _quid_, ignes.
Miseratus fonte Ditis conubia.

<--->

## Mid Content
Lorem markdownum insigne. Olympo signis Delphis! Retexi Nereius nova develat
stringit, frustra Saturnius uteroque inter!

<--->

## Right Content
Lorem markdownum insigne. Olympo signis Delphis! Retexi Nereius nova develat
stringit, frustra Saturnius uteroque inter! Oculis non ritibus Telethusa
protulit, sed sed aere valvis inhaesuro Pallas animam: qui _quid_, ignes.
Miseratus fonte Ditis conubia.
{{< /columns >}}

### 3.3. 折叠样式 Details

默认折叠：

```tpl
{{</* details title="Title 2" */>}}
### 默认折叠的内容
Lorem markdownum insigne...
{{</* /details */>}}
```

{{< details "Title 2" >}}
### 默认折叠的内容
Lorem markdownum insigne...
{{< /details >}}

默认打开展示：

```tpl
{{</* details "Title 1" open */>}}
### 默认打开展示的内容
Lorem markdownum insigne...
{{</* /details */>}}
```

{{< details "Title 1" open >}}
### 默认打开展示的内容
Lorem markdownum insigne...
{{< /details >}}

### 3.4. 展开样式 Expand

默认折叠样式：

```tpl
{{</* expand */>}}
## Markdown content
Lorem markdownum insigne...
{{</* /expand */>}}
```

{{< expand >}}
## Markdown content
Lorem markdownum insigne...
{{< /expand >}}

自定义标题和提示符的折叠样式：

```tpl
{{</* expand "Custom Label" "..." */>}}
## Markdown content
Lorem markdownum insigne...
{{</* /expand */>}}
```

{{< expand "Custom Label" "..." >}}
## Markdown content
Lorem markdownum insigne. Olympo signis Delphis! Retexi Nereius nova develat
stringit, frustra Saturnius uteroque inter! Oculis non ritibus Telethusa
protulit, sed sed aere valvis inhaesuro Pallas animam: qui _quid_, ignes.
Miseratus fonte Ditis conubia.
{{< /expand >}}

### 3.5. 特别提示 Hints

有三种不同的样式，分别是 `info`、`warning` 和 `danger`。

```tpl
{{</* hint [info|warning|danger] */>}}
**Markdown content**  
Lorem markdownum insigne. Olympo signis Delphis! Retexi Nereius nova develat
stringit, frustra Saturnius uteroque inter! Oculis non ritibus Telethusa
{{</* /hint */>}}
```

{{< hint info >}}
**Markdown content**  
Lorem markdownum insigne. Olympo signis Delphis! Retexi Nereius nova develat
stringit, frustra Saturnius uteroque inter! Oculis non ritibus Telethusa
{{< /hint >}}

{{< hint warning >}}
**Markdown content**  
Lorem markdownum insigne. Olympo signis Delphis! Retexi Nereius nova develat
stringit, frustra Saturnius uteroque inter! Oculis non ritibus Telethusa
{{< /hint >}}

{{< hint danger >}}
**Markdown content**  
Lorem markdownum insigne. Olympo signis Delphis! Retexi Nereius nova develat
stringit, frustra Saturnius uteroque inter! Oculis non ritibus Telethusa
{{< /hint >}}

### 3.6. 分页卡片 Tabs

可以用来展示不同平台或场景下对应的内容。

```tpl
{{</* tabs "uniqueid" */>}}
{{</* tab "macOS" */>}} # macOS Content {{</* /tab */>}}
{{</* tab "Linux" */>}} # Linux Content {{</* /tab */>}}
{{</* tab "Windows" */>}} # Windows Content {{</* /tab */>}}
{{</* /tabs */>}}
```

{{< tabs "uniqueid" >}}
{{< tab "macOS" >}}
# macOS

This is tab **macOS** content.

Lorem markdownum insigne. Olympo signis Delphis! Retexi Nereius nova develat
stringit, frustra Saturnius uteroque inter! Oculis non ritibus Telethusa
protulit, sed sed aere valvis inhaesuro Pallas animam: qui _quid_, ignes.
Miseratus fonte Ditis conubia.
{{< /tab >}}

{{< tab "Linux" >}}

# Linux

This is tab **Linux** content.

Lorem markdownum insigne. Olympo signis Delphis! Retexi Nereius nova develat
stringit, frustra Saturnius uteroque inter! Oculis non ritibus Telethusa
protulit, sed sed aere valvis inhaesuro Pallas animam: qui _quid_, ignes.
Miseratus fonte Ditis conubia.
{{< /tab >}}

{{< tab "Windows" >}}

# Windows

This is tab **Windows** content.

Lorem markdownum insigne. Olympo signis Delphis! Retexi Nereius nova develat
stringit, frustra Saturnius uteroque inter! Oculis non ritibus Telethusa
protulit, sed sed aere valvis inhaesuro Pallas animam: qui _quid_, ignes.
Miseratus fonte Ditis conubia.
{{< /tab >}}
{{< /tabs >}}

### 3.7. 示意图 Mermaid.js

可以插入使用 Mermaid.js 绘制的图表：

```tpl
{{</*/* mermaid */*/>}}
stateDiagram-v2
    State1: The state with a note
    note right of State1
        Important information! You can write
        notes.
    end note
    State1 --> State2
    note left of State2 : This is the note to the left.
{{</*/* /mermaid */*/>}}
```

{{< mermaid >}}
stateDiagram-v2
    State1: The state with a note
    note right of State1
        Important information! You can write
        notes.
    end note
    State1 --> State2
    note left of State2 : This is the note to the left.
{{< /mermaid >}}

### 3.8. 数学公式 Katex

可以使用 [KaTeX](https://katex.org/) 来渲染 TeX 数学公式：

{{< columns >}}

```latex
{{</*/* katex display */*/>}}
f(x) = \int_{-\infty}^\infty\hat f(\xi)\,e^{2 \pi i \xi x}\,d\xi
{{</*/* /katex */*/>}}
```

<--->

{{< katex display >}}
f(x) = \int_{-\infty}^\infty\hat f(\xi)\,e^{2 \pi i \xi x}\,d\xi
{{< /katex >}}

{{< /columns >}}

也可以内嵌显示：

Here is some inline example: {{< katex >}}\pi(x){{< /katex >}}, rendered in the same line. And below is `display` example, having `display: block`
{{< katex display >}}
f(x) = \int_{-\infty}^\infty\hat f(\xi)\,e^{2 \pi i \xi x}\,d\xi
{{< /katex >}}
Text continues here.

### 3.9. 子页面梗概 Sections

会把下一级标题和 `<!--more-->` 标记之前的部分作为梗概，点击标题会跳转到对应的内容。

```tpl
{{</* section */>}}
```

### 3.10. 更多自带的组件

更多的组件可以参考 [Hugo 官方文档](https://gohugo.io/content-management/shortcodes/)，比如展示 Tweet 推文的组件：

```tpl
{{</* tweet user="SanDiegoZoo" id="1453110110599868418" */>}}
```

{{< tweet user="SanDiegoZoo" id="1453110110599868418" >}}
