---
weight: 3
bookHidden: false
bookFlatSection: true
bookToc: false
title: "✍️ 文档编写指南"
---

# 介绍

这部分将简单的介绍文档编写的工作流和相关问题的解决方法。

## 文档图片

![Image](ok.png)

## 命令行操作

<script id="asciicast-m8KsjNReIMup9WJwbdjhL3zoi" src="https://asciinema.org/a/m8KsjNReIMup9WJwbdjhL3zoi.js" async></script>
