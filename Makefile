.PHONY: default

HUGO=hugo

default:
	@echo "build target is required for $(BINARY)"
	@exit 1

dev:
	$(HUGO) serve -v --debug --config "config.dev.toml"

prod:
	$(HUGO) serve -v --minify --environment=production --config "config.prod.toml"

build:
	$(HUGO) -v --minify --environment=production --config "config.prod.toml"

build_dev:
	$(HUGO) -v --debug --config "config.dev.toml"

clean:
	rm -rf public
